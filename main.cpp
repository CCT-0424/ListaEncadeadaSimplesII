#include <iostream>

using namespace std;

struct num {
 int numero;
 struct num *proximo;
};

num* inicio = NULL;

num* iniciar(){
    num *temp = new num;
    temp->numero=-1;
    temp->proximo=NULL;
    return temp;
}

void adicionar(num *inicio){
  num *temp = new num;
  cout << "Digite um número: ";
  cin >> temp->numero;
  temp->proximo = NULL;
  if (inicio->proximo == NULL){
     inicio->proximo = temp;
  } else {
     num *p = inicio->proximo;
     while (p->proximo != NULL){
        p = p->proximo;
     }
     p->proximo=temp;
  }
}

void inserir(num *inicio,int posicao){
    num * temp = new num;
    cout << "Digite um número: ";
    cin >> temp->numero;
    temp->proximo = NULL;

    num *p = inicio;
    int contador=0;
    while (p != NULL) {
       contador++;
       if (posicao == contador) {
            num *t = p->proximo;
            p->proximo = temp;
            temp->proximo = t;
       }
       p=p->proximo;
    }
}

void remover(num *inicio,int posicao){
    num *p = inicio;
    int contador=0;
    while (p) {
       contador++;
       if (posicao == contador) {
            num *t = p->proximo;
            p->proximo = t->proximo;
            delete t;
       }
       p=p->proximo;
    }
}

void imprimir(num *inicio){
    num* temp = inicio->proximo;
    while (temp) {
        cout << temp->numero << endl;
        temp = temp->proximo;
    }
}

int main()
{
    inicio = iniciar();
    adicionar(inicio);
    adicionar(inicio);
    adicionar(inicio);
    adicionar(inicio);
    inserir(inicio,5);
    imprimir(inicio);
    cout << endl;
    remover(inicio,4);
    imprimir(inicio);
    return 0;
}
